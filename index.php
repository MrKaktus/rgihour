<?php
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::before('start', function (&$params, &$output) {
    ORM::configure('sqlite:RGIHour.sqlite3');
});

/********************************************************************************/
/******************************************************************************* */

Flight::route('/RGIHour', function () {
    /*var_dump(Flight::request());*/ /*pour affichage de test*/
    $data = [
        'GetTableNomEleve' => Model::factory('GetNomEleve')->find_many(),
    ];
    Flight::view()->display('rgihour.twig', $data);
});

/***************************************************************************** */

Flight::route('/RGIHour/Connexion', function () {
    /*var_dump(Flight::request());*/
    $data = [
        'login' => GetCompte::find_many(),
    ];
    Flight::view()->display('connexion.twig', $data);
});

Flight::route('/RGIHour/Connexion/add', function () {

    if (Flight::request()->method == 'POST') {

        $pseudo = Flight::request()->data->pseudo;
        $mdp = Flight::request()->data->mdp;

        /*if (!connect($pseudo, $mdp)) {
            Flash::success(sprintf('Bienvenue %s vous êtes à présent connecté!', $pseudo->pseudo));
            Flight::redirect('/RGIHour');
        }
        else{
            Flash::warning('Ooops, identifiant ou mot de passe incorrect !');

        Flight::redirect('/RGIHour/Connexion');
        }*/
    }
    
});

/************************************************************************ */

Flight::route('/RGIHour/Resultats', function () {
    $data = [
        'GetTableSondage' => GetSondage::find_many(),
    ];
    Flight::view()->display('resultats.twig', $data);
});

Flight::route('/RGIHour/Resultats/add', function () {

    if (Flight::request()->method == 'POST') {
        $ip = Flight::request()->ip;
        $date = Flight::request()->data->date;

        if (!IsIPInDatabase($ip, $date)) {
            $ajoutsondage = Model::factory('GetSondage')->create();
            $ajoutsondage->nom_eleve = Flight::request()->data->nom_eleve;
            $ajoutsondage->heure = Flight::request()->data->heure;
            $ajoutsondage->ip = $ip;
            $ajoutsondage->date = $date;
            $ajoutsondage->save();
        }
    }
    Flight::redirect('/RGIHour/Resultats');
});

/*********************************************************************************** */

Flight::route('/RGIHour/Commentaires', function () {
    /*var_dump(Flight::request());*/
    $data = [
        'EspaceCom' => GetCom::find_many(),
    ];
    Flight::view()->display('commentaires.twig', $data);
});

Flight::route('/RGIHour/Commentaires/add', function () {

    if (Flight::request()->method == 'POST') {
        $ajoutcommentaire = Model::factory('GetCom')->create();
        $ajoutcommentaire->pseudo = Flight::request()->data->pseudo;
        $ajoutcommentaire->com = Flight::request()->data->com;
        
        $ajoutcommentaire->save();
    }
    Flight::redirect('/RGIHour/Commentaires');
});

/**************************************************************** */

Flight::route('/RGIHour/Contact', function () {
    Flight::view()->display('contact.twig');
});

/**************************************************************** */
Flight::start();
