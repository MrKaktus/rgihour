<?php
require "vendor/autoload.php";

//Get table sondage
class GetSondage extends Model {
    public static $_table = 'sondage';

    public function model()
    {
       return $this->belongs_to('GetNomEleve', 'nom_eleve')->find_one();
    }
}

//Get table nomeleve
class GetNomEleve extends Model
{
    public static $_table = 'nomeleve';
}

//Get table commentaire
class GetCom extends Model {
    public static $_table = 'commentaires';
}

//Get table compte pour connexion
class GetCompte extends Model {
    public static $_table = 'compte';
}